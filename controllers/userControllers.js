const User = require('./../models/User.js')
const Course = require('./../models/Course.js')
const bcrypt = require("bcrypt");
const auth = require("./../auth.js");


module.exports.checkEmail = (reqBody) => {

	return User.findOne( {email: reqBody.email}).then( (result, error) => {
		
		if(result != null) {
			return `Email already exists`
		} else {
			if(result) {
				return true
			} else {
				return error
			}
		}
	})
}

module.exports.register = (reqBody) => {
	
	return User.findOne( {email: reqBody.email}).then( (result, error) => {
		if(result != null){
			return `Email exists`
		} else {
			let newUser = new User({
				firstName: reqBody.firstName,
				lastName: reqBody.lastName,
				email: reqBody.email,
				password: bcrypt.hashSync(reqBody.password, 10),
				mobileNo: reqBody.mobileNo
			})

			//save()
			return newUser.save().then( (result, error) => {
				if(result){
					return true
				} else {
					return error
				}
			})
		}
	})
}

module.exports.getAllUsers = () => {

	return User.find().then( (result, error) => {

		if(result) {
			return result
		} else {
			return error
		}
	})
}

module.exports.login = (reqBody) => {

	const {email, password} = reqBody;

	return User.findOne({email: email}).then( (result, error) => {

		if(result == null){
			return false
		} else {
			//what if we found the email and is existing, but the pw is incorrect
			let isPasswordCorrect = bcrypt.compareSync(password, result.password)

			if(isPasswordCorrect == true){
				return {access: auth.createAccessToken(result)}
			} else {
				return false
			}
		}
	})
}

module.exports.getProfile = (data) => {
	// console.log(data)
	const {id} = data

	return User.findById(id).then((result, err) => {
		// console.log(result)

		if(result != null){
			result.password = "";
			return result
		} else {
			return false
		}
	})
}

module.exports.enroll = async (data) => {

	const {userId, courseId} = data

	//look for matching document of a user
	const userEnroll = await User.findById(userId).then( (result, error) => {
		if(error) {
			return error
		} else {
			//console.log(result)
			
			//add the courseId in the enrollments array
			result.enrollments.push( {courseId: courseId})

			// console.log(result)

			if(result.enrollments.courseId != courseId) {
				//save the changes made to the document
				return result.save().then( (result, error) => {
					if(error){
						return error
					} else {
						return true
					}
				})
			} else {
				return false
			}

		}
	})

	//look for matching document of a course
	const courseEnroll = await Course.findById(courseId).then( (result, error) => {
		if(error) {
			return error
		} else {
			//console.log(result)

			//add the userId in the enrollees array
			result.enrollees.push( {userId: userId})

			if(result.enrollees.userId != userId) {
				//save the changes made to the document
				return result.save().then( (result, error) => {
					if (error) {
						return error
					} else {
						return true
					}
				})
			} else {
				return false
			}
		}
	})

	//to return only one value for the function enroll
	if(userEnroll && courseEnroll){
		return true
	} else {
		return false
	}
}