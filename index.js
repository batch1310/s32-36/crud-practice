
//import express module
const express = require("express");
const mongoose = require("mongoose");
const app = express();

const PORT = process.env.PORT || 4000; 

const courseRoutes = require('./routes/courseRoutes.js');
const userRoutes = require('./routes/userRoutes.js');

mongoose.connect('mongodb+srv://marcalvaira92:dec61992@batch139.qfcds.mongodb.net/course-booking?retryWrites=true&w=majority',
	{useNewUrlParser: true, useUnifiedTopology: true}
);

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once("open", () => console.log(`Connected to database`));

//Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}))

//Routes
app.use("/api/courses", courseRoutes);
app.use("/api/users", userRoutes);




app.listen(PORT, () => console.log(`Server running at port ${PORT}`));